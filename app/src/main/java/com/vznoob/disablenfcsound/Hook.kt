package com.vznoob.disablenfcsound

import de.robv.android.xposed.IXposedHookLoadPackage
import de.robv.android.xposed.XC_MethodHook
import de.robv.android.xposed.XposedBridge
import de.robv.android.xposed.XposedHelpers.findAndHookMethod
import de.robv.android.xposed.callbacks.XC_LoadPackage

class Hook : IXposedHookLoadPackage {
    override fun handleLoadPackage(lpparam: XC_LoadPackage.LoadPackageParam) {
        if (lpparam.packageName != "com.android.nfc") {
            return
        }

        try {
            findAndHookMethod(
                lpparam.classLoader.loadClass("com.android.nfc.NfcService"),
                "playSound",
                Int::class.java,
                object : XC_MethodHook() {
                    override fun beforeHookedMethod(param: MethodHookParam) {
                        if (param.args[0] == 1) {
                            param.result = null
                        }
                    }
                }
            )
        } catch (e: ClassNotFoundException) {
            XposedBridge.log(e)
        }
    }
}